from django.urls import include, path
from . import views

urlpatterns = [
    path('story8/', views.index, name='index'),
    path('data/', views.searchbook, name='searchbook'),

]
