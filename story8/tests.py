from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import index
from .apps import Story8Config
from django.apps import apps

# Create your tests here.

class UrlsTest8(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code,200)
        
    def test_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response,'story8/index.html')

    def test_event_searchbook_url_is_exist(self):
        response = Client().get('/data/?q=book')
        self.assertEqual(response.status_code, 200)
