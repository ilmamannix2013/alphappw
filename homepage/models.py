from django.db import models

class Course(models.Model):
    course = models.CharField(max_length=50)
    lecturer = models.CharField(max_length=50)
    sks = models.IntegerField(null=True)
    desc = models.TextField(null=True)
    term = models.CharField(max_length=50,null=True)
    classroom = models.CharField(max_length=30,null=True)


    def __str__(self):
        return "{}.{}".format(self.id,self.course)

# Create your models here.
