from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('skill', views.skill, name='skill'),
    path('index', views.index, name='index'),
    path('list',views.list,name='list'),
    path('create',views.create,name='create'),
    path('delete/<int:delete_id>',views.delete,name='delete'),
    path('detail/<int:id_detail>', views.detail, name='detail'),
]