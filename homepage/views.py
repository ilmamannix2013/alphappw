from django.shortcuts import render, redirect,get_object_or_404
from .models import Course
from .forms import CourseForm

def index(request):
    return render(request, 'index.html')
def skill(request):
    return render(request,'skill.html')
def list(request):
    all_course = Course.objects.all()
    context = {
        'page_title':'Courses',
        'all_course':all_course
    }
    return render(request,'courses.html',context)

def create(request):
    course_form = CourseForm(request.POST or None)

    if request.method == 'POST' :
        if course_form.is_valid():
            course_form.save()
        return redirect('homepage:list')

    context = {
        "page_title":"Add Course",
        "course_form":course_form,
    }
    return render(request,'create.html',context)

def delete(request,delete_id):
    Course.objects.filter(id=delete_id).delete()
    return redirect('homepage:list')

def detail(request, id_detail) :
   id_course = Course.objects.get(id=id_detail)
   return render(request, 'detail.html', {'course': id_course})

# Create your views here.
