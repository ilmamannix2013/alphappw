$(document).ready(function () {
    $(".card-body").hide();
    $(".card-header").click(function () {
        var id = this.id;   /* getting heading id */
        /* looping through all elements which have class .card-body */
        $(this).next().toggle("slow");  /* Selecting div after h1 */
    });


    $(".up").click(function () {
        var $current = $(this).closest('.card')
        var $previous = $current.prev('.card');
        if ($previous.length !== 0) {
            $current.insertBefore($previous).delay("slow").fadeIn();
        }
        return false;
    });

    $(".down").click(function () {
        var $current = $(this).closest('.card')
        var $next = $current.next('.card');
        if ($next.length !== 0) {
            $current.insertAfter($next).delay("slow").fadeIn();
        }
        return false;
    });
});
