from .apps import Story7Config
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User
from .forms import LoginForm, RegisterForm
from django.contrib.auth import authenticate, login, logout

class Story7UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code,200)
    def test_template_activities(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response,'story7/accordion.html')

class Story9UnitTest(TestCase):
    def test_signup_url_exists(self):
        response = Client().get('/story7/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_register(self):
        response = Client().get('/story7/register/')
        self.assertTemplateUsed(response,'story7/register.html')

    def test_logout_url_exist(self):
        response = Client().get('/story7/logout/')
        self.assertEqual(response.status_code, 302)

    def test_login_url_exist(self):
        response = Client().get('/story7/login/')
        self.assertEqual(response.status_code, 200)

    def test_template_login(self):
        response = Client().get('/story7/login/')
        self.assertTemplateUsed(response,'story7/login.html')

    def test_forms_login_valid(self):
        form_login = LoginForm(data={
            "username": "testing",
            "password": "testing123"
        })
        self.assertTrue(form_login.is_valid())

    def test_forms_login_invalid(self):
        form_login = LoginForm(data={
            "username": "test",
            "password": ""
        })
        self.assertFalse(form_login.is_valid())
        
    def test_forms_register_valid(self):
        form1 = RegisterForm(data={
            "username": "",
            "email": "tested@gmail.com",
            'password1': 'tested123',    
        })
        self.assertFalse(form1.is_valid())
    
    def test_POST_login_valid(self):
        response = Client().post('/story7/login/',
                                    {
                                        'username': 'study',
                                        'password ': "studyPPW"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_login_invalid(self):
        response = Client().post('/story7/login/',
                                    {
                                        'username': "",
                                        'password ': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'story7/login.html')
    
    def test_POST_reg_valid(self):
        response = Client().post('/story7/register/',
                                    {
                                        "username": "testingg",
                                        "email": "testingg@gmail.com",
                                        'password1': 'testingg123',
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_reg_invalid(self):
        response = Client().post('/story7/register/',
                                    {"username": "",
                                    "email": "hagmail.com",
                                    'password1': 'testingg',
                                    }, follow=True)
        self.assertTemplateUsed(response, 'story7/register.html')