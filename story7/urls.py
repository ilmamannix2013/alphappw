from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('',views.accord,name='accord'),
    path('register/', views.register, name="register"),
     path('login/', views.loginCus, name='login'),
    path('logout/', views.logoutCus, name='logout'),
]