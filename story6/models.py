from django.db import models

# Create your models here.

class listKegiatan(models.Model):
	namaKegiatan = models.TextField(max_length=300)

	def __str__(self):
		return self.namaKegiatan

class listSiswa(models.Model):
	namaSiswa = models.TextField(max_length=300)
	kegiatan = models.ForeignKey(listKegiatan, on_delete=models.CASCADE)

	def __str__(self):
		return self.namaSiswa



# Create your models here.
