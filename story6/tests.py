from django.test import TestCase, Client # Jangan lupa import Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
import time

class Story6UnitTest(TestCase):
    #Cek url /story6 ada atau tidak
    def test_url_is_exist(self):
        response = Client().get('/story6')
        self.assertEqual(response.status_code,200)
    
    #Cek apakah story6 memakai template kegiatan.html yang berada di folder story6
    def test_template_activities(self):
        response = Client().get('/story6')
        self.assertTemplateUsed(response,'story6/kegiatan.html')
    
    def test_url_using_func(self):
        found = resolve('/story6')
        self.assertEqual(found.func, createKegiatan)

    def test_create_models(self):
        new_model = listKegiatan.objects.create(namaKegiatan ='sabeb', id=20)
        new_model2 = listSiswa.objects.create(namaSiswa = 'siswa', kegiatan = new_model)
        counting_new_model = listKegiatan.objects.all().count()
        self.assertEqual(str(new_model), new_model.namaKegiatan)
        self.assertEqual(str(new_model2), new_model2.namaSiswa)
        self.assertEqual(counting_new_model,1)
        post = self.client.post('/story6create/20',data={'namaSiswa':'siswa'})

    def test_form_is_blank(self):
        form = formKegiatan(data= {'namaKegiatan':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['namaKegiatan'],
            ["This field is required."]
            )

    def test_post_form(self):
        response = Client().post('/story6',{'namaKegiatan':'hai'})
        self.assertIn('hai',response.content.decode())

    def test_form_is_blank_siswa(self):
        form = formSiswa(data= {'namaSiswa':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['namaSiswa'],
            ["This field is required."]
            )



# Create your tests here.
