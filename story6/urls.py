from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('',views.createKegiatan,name='kegiatan'),
    path('create/<int:siswa_id>', views.createSiswa, name='create'),
]